module.exports = function(grunt) {

    // 1. All configuration goes here 
    grunt.initConfig({
        vars: {
			source: 'source',
			release: 'release',
            tmp: '.tmp',
			title: 'Dev'
        }
    });

    grunt.loadTasks('grunt');
    
    grunt.registerTask('default', ['mkdir', 'env:dev', 'sass:dev', 'handlebars', 'concat', 'copy']);
    grunt.registerTask('release', ['mkdir', 'env:release', 'sass:release', 'handlebars', 'concat', 'uglify', 'copy']);
	grunt.registerTask('combine', ['concat']);
    grunt.registerTask('min', ['uglify']);
};