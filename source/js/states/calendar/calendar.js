App.state('calendar', {
	autoDomEl : false,
	setup : function(state, data) {
		/* 
		 * Start simulated data
		 * Since this is just local files running in a browser, I'm just returning a static object that would have been a stringified JSON response
	     * All dates would be UTC in real life and some flags like "otherMonth," "current," and "past" would be handled on the client so wouldn't really exist
		 */

		state.setData('day', {
	    	rangeDate : 'Thursday, June 18th, 2015',
	    	headings : ['Thursday'],
	    	label : 'Remote Calendar 1',
	    	type : 'day',
	    	dates : [
				{date : 18, events : [{type : 'email',time : '12p',content : 'Dreamforce Prep Work'},{type : 'webinar',time : '2p',content : 'Meet-n-Greet w/ New Tech'},{type : 'webinar',time : '8p',content : 'Get the 411 on the 311'},{type : 'cloud',time : '11p',content : 'Site Visit Down the Street'},{type : 'webinar',time : '2p',content : 'Meet-n-Greet w/ New Tech'},{type : 'webinar',time : '8p',content : 'Get the 411 on the 311'},{type : 'cloud',time : '11p',content : 'Site Visit Down the Street'}], current : true}
	    	]
	    });
	    
	    state.setData('week', {
	    	rangeDate : 'June 14th - 20th, 2015',
	    	headings : ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
	    	label : 'Remote Calendar 1',
	    	type : 'week',
	    	dates : [
	    		{date : 14, past : true},
				{date : 15, past : true},
				{date : 16, events : [{type : 'webinar',time : '2p',content : 'How to Win at Marketing'},{type : 'email',time : '12p',content : 'Dreamforce Prep Work'}], past : true},
				{date : 17, past : true},
				{date : 18, events : [{type : 'email',time : '12p',content : 'Dreamforce Prep Work'},{type : 'webinar',time : '2p',content : 'Meet-n-Greet w/ New Tech'},{type : 'webinar',time : '8p',content : 'Get the 411 on the 311'},{type : 'cloud',time : '11p',content : 'Site Visit Down the Street'},{type : 'webinar',time : '2p',content : 'Meet-n-Greet w/ New Tech'},{type : 'webinar',time : '8p',content : 'Get the 411 on the 311'},{type : 'cloud',time : '11p',content : 'Site Visit Down the Street'}], current : true},
				{date : 19},
				{date : 20}
	    	]
	    });

	    state.setData('month', {
			rangeDate : 'June 2013',
			label : 'Remote Calendar 1',
			type : 'month',
			multiDayEvents : [
				{startDate : 8, endDate : 11, type : 'cloud',time : '',content : 'Customer Site Visit: Sony, Chicago'},
				{startDate : 29, endDate : 2, type : 'cloud',time : '',content : 'Customer Site Visit: Sony, Chicago'}
			],
			headings : ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
			dates : [
				{date : 31, otherMonth : true, past : true},
				{date : 1, past : true},
				{
					date : 2,
					events : [
						{type : 'email',time : '12p',content : 'Dreamforce Prep Work'},
						{type : 'webinar',time : '2p',content : 'Meet-n-Greet w/ New Tech'},
						{type : 'webinar',time : '8p',content : 'Get the 411 on the 311'},
						{type : 'cloud',time : '11p',content : 'Site Visit Down the Street'},
						{type : 'webinar',time : '2p',content : 'Meet-n-Greet w/ New Tech'},
						{type : 'webinar',time : '8p',content : 'Get the 411 on the 311'},
						{type : 'cloud',time : '11p',content : 'Site Visit Down the Street'}
					], 
					past : true
				},
				{date : 3, events : [{type : 'webinar',time : '8p',content : 'Get the 411 on the 311'}], past : true},
				{date : 4, events : [{type : 'webinar',time : '8a',content : 'Get the 411 on the 311'}, {type : 'email',time : '12p',content : 'How to Win at Marketing'}], past : true},
				{date : 5, past : true},
				{date : 6, past : true},
				{date : 7, past : true},
				{date : 8, events : [{type : 'email',time : '12p',content : 'Dreamforce Prep Work'}, {type : 'email',time : '12p',content : 'Dreamforce Prep Work'},{type : 'webinar',time : '2p',content : 'Meet-n-Greet w/ New Tech'}], past : true},
				{date : 9, past : true},
				{date : 10, events : [{type : 'webinar',time : '2p',content : 'How to Win at Marketing'},{type : 'email',time : '12p',content : 'Dreamforce Prep Work'}], past : true},
				{date : 11, past : true},
				{date : 12, past : true},
				{date : 13, past : true},
				{date : 14, past : true},
				{date : 15, past : true},
				{date : 16, events : [{type : 'webinar',time : '2p',content : 'How to Win at Marketing'},{type : 'email',time : '12p',content : 'Dreamforce Prep Work'}], past : true},
				{date : 17, past : true},
				{date : 18, events : [{type : 'email',time : '12p',content : 'Dreamforce Prep Work'},{type : 'webinar',time : '2p',content : 'Meet-n-Greet w/ New Tech'},{type : 'webinar',time : '8p',content : 'Get the 411 on the 311'},{type : 'cloud',time : '11p',content : 'Site Visit Down the Street'},{type : 'webinar',time : '2p',content : 'Meet-n-Greet w/ New Tech'},{type : 'webinar',time : '8p',content : 'Get the 411 on the 311'},{type : 'cloud',time : '11p',content : 'Site Visit Down the Street'}], current : true},
				{date : 19},
				{date : 20},
				{date : 21},
				{date : 22},
				{date : 23, events : [{type : 'email',time : '12p',content : 'Dreamforce Prep Work'},{type : 'webinar',time : '2p',content : 'Meet-n-Greet w/ New Tech'},{type : 'webinar',time : '8p',content : 'Get the 411 on the 311'},{type : 'cloud',time : '11p',content : 'Site Visit Down the Street'},{type : 'webinar',time : '2p',content : 'Meet-n-Greet w/ New Tech'},{type : 'webinar',time : '8p',content : 'Get the 411 on the 311'},{type : 'cloud',time : '11p',content : 'Site Visit Down the Street'}]},
				{date : 24, events : [{type : 'webinar',time : '2p',content : 'Meet-n-Greet w/ New Tech'}]},
				{date : 25, events : [{type : 'webinar',time : '2p',content : 'How to Win at Marketing'},{type : 'email',time : '12p',content : 'Dreamforce Prep Work'}]},
				{date : 26},
				{date : 27},
				{date : 28},
				{date : 29, events : [{type : 'cloud',time : '11p',content : 'Site Visit Down the Street'},{type : 'email',time : '12p',content : 'Dreamforce Prep Work'},{type : 'webinar',time : '2p',content : 'Meet-n-Greet w/ New Tech'},{type : 'webinar',time : '8p',content : 'Get the 411 on the 311'},{type : 'cloud',time : '11p',content : 'Site Visit Down the Street'},{type : 'webinar',time : '2p',content : 'Meet-n-Greet w/ New Tech'},{type : 'webinar',time : '8p',content : 'Get the 411 on the 311'},{type : 'cloud',time : '11p',content : 'Site Visit Down the Street'}]},
				{date : 30},
				{date : 1, otherMonth : true},
				{date : 2, otherMonth : true},
				{date : 3, events: [{type : 'webinar',time : '8p',content : 'Get the 411 on the 311'},{type : 'cloud',time : '11p',content : 'Site Visit Down the Street'}], otherMonth : true},
				{date : 4, otherMonth : true}
			]
		});
	},

	enter : function(state, data) {
		var monthData = state.getData('month');
		state.domEl = App.createGet(App.Templates.calendarDetail(monthData), {isTemplate : true, replace : true}, '#content');
	},

	setView : function(range) {
		var state = App.getState('calendar');
		var rangeData = state.getData(range);
		state.domEl = App.createGet(App.Templates.calendarDetail(rangeData), {isTemplate : true, replace : true}, '#content');
	}
});