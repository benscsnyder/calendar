App.state('modal', {
	autoDomEl : false,
	appendClass : false,
	setup : function(state, data) {
		state.domEl = App.createGet(App.Templates.modalModal(), {isTemplate : true}, 'body');
	},

	enter : function(state, data) {
		var target = document.getElementById('modal-target');
		var drop = document.getElementById('modal-backdrop');

		App.addClass(state.domEl, 'pcal-modal--visible');
		App.addClass(drop, 'pcal-modal--visible');
		App.addClass(target, 'slds-fade-in-open');

		document.addEventListener('keydown', this.handleKeys);
	},

	exit : function(state, data) {
		var target = document.getElementById('modal-target');
		var drop = document.getElementById('modal-backdrop');
		var content = document.getElementById('modal-content');

		App.removeClass(state.domEl, 'pcal-modal--visible');
		App.removeClass(drop, 'pcal-modal--visible');
		App.removeClass(target, 'slds-fade-in-open');

		content.innerHTML = '';

		document.removeEventListener('keydown', this.handleKeys);
	},

	handleKeys : function(event) {
		var keyCode = event.keyCode;

		switch(keyCode) {
			case 27: // escape
				App.exit('modal');
				break;
		}
	}
});