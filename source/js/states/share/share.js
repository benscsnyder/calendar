App.state('share', {
	autoDomEl : false,
	appendClass : false,
	setup : function(state) {
		state.setData('data', {
			shares : [
				{
					email : 'dtargaryen@got.com',
					rights : 'read'
				},
				{
					email : 'tlannister@got.com',
					rights : 'read'
				},
				{
					email : 'astark@got.com',
					rights : 'write'
				}
			]
		});

		state.setData('whitelist', ['got.com', 'sony.com']);
		state.setData('tempShares', []);
	},
	enter : function(state, data) {
		var stateData = state.getData('data');

		App.set('modal');
		App.createGet(App.Templates.shareModalContent(stateData), {isTemplate : true}, '#modal-content');
	},

	exit : function(state, data) {
		App.exit('modal');
	},

	add : function() {
		var state = App.getState('share');
		var inputEmail = document.getElementById('share-user-email');
		var newEmail = App.encode(inputEmail.value);
		var tempShares;
		var existingShares;
		var internal;
		var rights;
		var cls;
		var html;
		var newShare;
		var newDom;
		var slideTo;

		if (!newEmail) {
			alert('Whoops! You need to add an email.');
			return;
		} else if (newEmail.indexOf('@') === -1) { 
			// not an adequate email validation check!
			alert('Not a valid email.');
			return;
		}

		tempShares = state.getData('tempShares');
		tempShares.forEach(function(share) {
			if (share.email === newEmail) {
				newEmail = null;
				return;
			}
		});

		existingShares = state.getData('data').shares;
		existingShares.forEach(function(share) {
			if (share.email === newEmail) {
				newEmail = null;
				return;
			}
		});

		if (newEmail === null) {
			alert('This calendar is already shared with that user.');
			return;
		}

		internal = (function isUserInternal(em) {
			// This would ideally be an ajax request to a script that checks
			// the DB to see if the user is a known user. Faking it with
			// poor man's email extension check
			var whitelist = state.getData('whitelist');
			var emExt = em.split('@')[1];
			var ret = false;

			whitelist.forEach(function(item) {
				if (item === emExt) {
					ret = true;
					return;
				}
			});

			return ret;
		})(newEmail);

		if (!internal && !confirm("This user is not a part of your organization. Share with them anyway?")) {
			return;
		}

		rights = document.getElementById('share-user-rights').value;
		html = '<strong href="">' + newEmail + '</strong>';
		cls = 'pcal-share pcal-share--collapsed pcal-share--' + rights;
		
		newDom = App.createGet('a', {
			prepend : true,
			className : cls,
			innerHTML : html,
			href : ''
		}, '#share-list');


		slideTo = window.setTimeout(function() {
			App.removeClass(newDom, 'pcal-share--collapsed');
			window.clearTimeout(slideTo);
		}, 50);

		inputEmail.value = '';

		// update local state data
		tempShares.push({
			email : newEmail,
			rights : rights
		});
		state.setData('tempShares', tempShares);
	},

	save : function() {
		var state = App.getState('share');
		var tempShares = state.getData('tempShares');
		var existingShares = state.getData('data').shares;

		if (tempShares.length > 0) {
			Array.prototype.push.apply(tempShares.reverse(), existingShares);

			state.setData('data', {
				shares : tempShares
			});
			state.setData('tempShares', []);

			// New AppState feature!
			App.fire('showToast', {
				type : 'success',
				message : 'Nice work! You shared a calendar.'
			});
		}

		App.exit('share');
	}
});