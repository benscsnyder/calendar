App.state('toast', {
	autoDomEl : false,
	appendClass : false,
	setup : function(state, data) {
		state.domEl = App.createGet(App.Templates.toastToast(), {isTemplate : true}, 'body');
		
		App.on('showToast', function(data) {
			App.set('toast', data);
		});
	},

	enter : function(state, data) {
		var outTo = window.setTimeout(function toastOut() {
			App.exit('toast');
			window.clearTimeout(outTo);
		}, data.delay ? data.delay : 2000);

		App.removeClass(state.domEl, 'pcal-toast--hidden');
		state.domEl.innerHTML = data.message;
	},

	exit : function(state, data) {
		App.addClass(state.domEl, 'pcal-toast--hidden');
	}
});