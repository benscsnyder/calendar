App.state('newEvent', {
	autoDomEl : false,
	appendClass : false,
	enter : function(state, data) {
		App.set('modal');
		App.createGet(App.Templates.newEventModalContent(), {isTemplate : true}, '#modal-content');
	},

	exit : function(state, data) {
		App.exit('modal');
	}
});