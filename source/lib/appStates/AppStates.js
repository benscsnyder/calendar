var App = window.App ? window.App : (function() {
	var _states = {},
		_data = {},
		exitStates = [],
		$body = document.body,
		tmpCache = {},

		addClass = function(el, str) {
			var className = el.className,
				spacer = ' ';

			if (className.indexOf(str) === -1) {
				if (el.className.length === 0) {
					spacer = '';
				}

				el.className += spacer + str;
			}
		},

		removeClass = function(el, str) {
			var className = el.className;

			className = className.replace(' ' + str, '');
			className = className.replace(str, '');

			el.className = className;
		},


		register = function(name, obj, isMainState) {
			// TODO: The properties of a state should be in the prototype
			_states[name] = new State(name, isMainState);

			for (var prop in obj) {
			    if (obj.hasOwnProperty(prop)) {
			        _states[name][prop] = obj[prop];
			    }
			}

			if (obj.setup) {
				_states[name].setup(_states[name]);
			}

			return _states[name];
		},

		setData = function(name, obj, state) {
			if (state) {
				// store the value internally to the state
				if (!state._data) {
					state._data = {};
				}

				state._data[name] = obj;

				return state._data[name];
			} else {
				_data[name] = obj;

				return _data[name];
			}
		},

		getData = function(name, state) {
			if (state && state._data && state._data[name]) {
				return state._data[name];
			} else if (typeof _data[name] !== 'undefined') {
				return _data[name];
			} else {
				return null;
			}
		},

		// TODO: add on, off, and fire to state objects
		// and call with state object/this context
		on = function(evtName, callback) {
			if (typeof callback === 'function') {
				var cont = true;

				if (!this.subscribers) {
					this.subscribers = [];
				}

				this.subscribers.forEach(function subOnLoop(sub) {
					// Listener is already attached
					if (sub.func === callback) {
						cont = false;
						return;
					}
				}.bind(this));
				
				if (cont) {
					this.subscribers.push({
						label : evtName,
						func : callback
					});
				}
			}
		},

		off = function(evtName, callback) {
			if (this.subscribers && typeof callback === 'function') {
				this.subscribers.forEach(function subOffLoop(sub, idx) {
					this.subscribers.splice(idx, 1);
				}.bind(this));
			}
		},

		fire = function(evtName, payload) {
			if (this.subscribers) {
				this.subscribers.forEach(function subFireLoop(sub, idx) {
					if (sub.label === evtName) {
						sub.func(payload);
					}
				}.bind(this));
			}
		},

		/*
		Options {
			autoDomEl : Bool // query the dom for a dom element with the ID of the state name
			visual : Bool // indicates a state that has visual elements associated with it
			appendClass : Bool // whether or not to add the state's name as a class to the body tag and the class of "current" to the state.domEl
		}
		*/
 
		State = function(viewId, isMainState) {
			this.viewId = viewId;
			this.isMainState = isMainState ? isMainState : false;
		}

		State.prototype.firstDone = false;
		State.prototype.appendClass = false;
		State.prototype.active = false;
		State.prototype.exitOnReEnter = false;

		State.prototype.prep = function(data) {
			var _state = this,
				$state = document.getElementById(_state.viewId),
				exitState;

			if (!_state.firstDone && _state.once) {
				_state.once(_state, data);
				_state.firstDone = true;
			}

			if (typeof _state.extend === 'string' && _states[_state.extend]) {
				_state.parentState = _states[_state.extend];
			}

			if (exitStates.length > 0) {
				for (var i = exitStates.length - 1; i >= 0; i--) {
					exitState = exitStates[i];

					if (exitState.exit) {
						exitState.exit();
					}
					
					exitState.deprep();
				};

				exitStates = [];
			}

			if (_state.visual !== false) {
				if (_state.appendClass !== false) {
					addClass($body, _state.viewId);
				}

				if ($state && _state.autoDomEl !== false) {
					_state.domEl = $state;
				}

				if (_state.appendClass !== false && _state.domEl) {
					addClass(_state.domEl, 'current');
				}
			}

			if (_state.alwaysExit) {
				exitStates.push(_state);
			}

			_state.active = true;
		};

		State.prototype.deprep = function() {
			var _state = this,
				$body = document.body;

			if (_state.visual !== false) {
				if (_state.appendClass !== false) {
					removeClass($body, _state.viewId);
				}
			}

			if (_state.domEl && _state.appendClass !== false) {
				removeClass(_state.domEl, 'current');
			}

			_state.active = false;
		};

		State.prototype.setData = function(name, data) {
			var _state = this;
			return setData(name, data, _state);
		};

		State.prototype.getData = function(name) {
			var _state = this;
			return getData(name, _state);
		};

	return {
		setData : function(name, obj) {
			return setData(name, obj);
		},

		getData : function(name) {
			return getData(name);
		},

		state : function(name, obj) {
			register(name, obj, true);
		},

		subState : function(name, obj) {
			register(name, obj, false);
		},

		set : function(name, obj) {
			var _state = _states[name];

			if (typeof _state !== 'undefined') {
				if (_state.exitOnReEnter && _state.active) {
					this.exit(name, obj);
				} else {
					if (_state.enter) {
						_state.enter(_state, obj);
					}

					if (_state.prep) {
						_state.prep(obj);
					}
				}
			}

			return _state;
		},

		getState : function(name) {
			var _state = _states[name];

			if (_state) {
				return _state;
			} else {
				return null;
			}
		},

		exit : function(name, obj) {
			var _state = _states[name];

			if (typeof _state !== 'undefined') {
				_state.deprep();

				if (_state.exit) {
					_state.exit(_state, obj);
				}
			}
		},

		addClass : function(el, cls) {
			addClass(el, cls);
		},

		removeClass : function(el, cls) {
			removeClass(el, cls);
		},

		/*
		App.createGet('div', {
			className : 'dialog',
			isTemplate : bool // whether the first param is a template string
			... properties of the div ...
		}, 'body')

		// Note: if passing a template string, in order to append to a target element, the template
		// must have a single parent element
		*/

		createGet : function(tag, config, target) {
			var sel = config.id ? config.id : config.className ? config.className : null,
				target,
				dom;

			function doAppend(d, t) {
				if (target) {
					target = typeof target === 'string' ? document.querySelector(target) : target;

					if (config.replace) {
						target.innerHTML = '';
					}

					if (config.prepend && target.insertAdjacentHTML) {
						var tmp = document.createElement('div');
						
						if (!d.id) {
							d.id = 'tmp' + new Date().getTime();
						}

						tmp.appendChild(d);
						target.insertAdjacentHTML('afterbegin', tmp.innerHTML);

						// Since insertAdjacentHTML creates a new dom el, need to query again
						return document.getElementById(d.id);
					} else {
						target.appendChild(d);
					}

					return d;
				}

				return null;
			}

			if (sel) {
				dom = document.querySelector(sel);

				if (!dom) {
					dom = document.createElement(tag);

					for (var prop in config) {
						if (config.hasOwnProperty(prop) && prop in dom) {
							dom[prop] = config[prop];
						}
					}

					dom = doAppend(dom, tag);
				}

				return dom;
			} else if (config.isTemplate) {
				dom = document.createElement('div');
				dom.innerHTML = tag;

				dom = doAppend(dom.firstChild);

				return dom;
			} else {
				return null;
			}
		},

		encode : function(str) {
			var div = document.createElement('div');
		    div.appendChild(document.createTextNode(str));
		    return div.innerHTML;
		},

		decode : function(str){
		    var div = document.createElement('div');
		    div.innerHTML = str;
		    return div.firstChild.nodeValue;
		},

		newHelper : function(name, func) {
			if (!this.helpers) {
				this.helpers = {};
			}

			if (!this.helpers[name]) {
				this.helpers[name] = func;
			}
		},

		on : function(evtName, callback) {
			on.call(this, evtName, callback);
		},

		off : function(evtName, callback) {
			off.call(this, evtName, callback);
		},

		fire : function(evtName, payload) {
			fire.call(this, evtName, payload);
		}
	}
})();