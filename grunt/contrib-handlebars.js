module.exports = function(grunt) {
	grunt.config('handlebars', {
		options : {
			namespace : 'App.Templates',
			processName: function(filePath) {
	            var pieces = filePath.split('/'),
	            	dirName = pieces[pieces.length-2],
	            	fileName = pieces[pieces.length - 1].split('.')[0];
	            
	            return dirName + fileName.charAt(0).toUpperCase() + fileName.slice(1);
	          }
		},
		compile: {
	        src: '<%= vars.source %>/js/states/**/*.hbs',
	        dest: '<%= vars.tmp %>/tmpls.js'
	      }
	    
	});

	grunt.loadNpmTasks('grunt-contrib-handlebars');
};