module.exports = function(grunt) {
	grunt.config('concat', {
		dist: {
			src: [
				'<%= vars.tmp %>/tmpls.js',
				'<%= vars.source %>/js/states/**/*.js',
				'<%= vars.source %>/js/*.js'
			],
			dest: '<%= vars.release %>/js/main.js'
		}
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
};