module.exports = function(grunt) {
	grunt.config('watch', {
		css : {
			files: ['<%= vars.source %>/css/**/*.scss'],
			tasks: ['sass'],
			options: {
				spawn: false
			}
		},
		scripts : {
			files: ['<%= vars.source %>/**/*.js'],
			tasks: ['concat'],
			options: {
				spawn: false
			}
		},
		hbs : {
			files: ['<%= vars.source %>/**/*.hbs'],
			tasks: ['handlebars', 'concat'],
			options: {
				spawn: false
			}
		},
		php : {
			files: ['<%= vars.source %>/*.html'],
			tasks: ['copy'],
			options : {
				spawn : false
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
};