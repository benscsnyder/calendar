module.exports = function(grunt) {
	grunt.config('sass', {
		dev : {
			options: {
				style: 'compressed'
			},
			files: {
				'<%= vars.release %>/css/lightning/index.css' : '<%= vars.source %>/css/lightning/index.scss',
				'<%= vars.release %>/css/root.css' : '<%= vars.source %>/css/root.scss'
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
};