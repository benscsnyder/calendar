module.exports = function(grunt) {
	grunt.config('copy', {
		release : {
			files : [
				{
					expand : true,
					cwd : '<%= vars.source %>/images/',
					src : '**',
					dest : '<%= vars.release %>/images/'
				},
				{
					expand : true,
					cwd : '<%= vars.source %>/css/lightning/assets/',
					src : '**',
					dest : '<%= vars.release %>/css/lightning/assets/'
				},
				{
					expand : true,
					cwd : '<%= vars.source %>/lib/',
					src : '**',
					dest : '<%= vars.release %>/lib/'
				},
				{
					expand : true,
					flatten : true,
					src : ['<%= vars.source %>/*.html'],
					dest : '<%= vars.release %>/'
				}
			]
		}
	});

	grunt.loadNpmTasks('grunt-contrib-copy');
};