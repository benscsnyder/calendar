## Install ##

1. clone repository
2. open terminal
3. cd into root directory
4. run "npm install" (no quotes)
5. run "grunt" (no quotes)
6. open release/index.html in a modern web browser

Follow any prompts to install missing packages or tools (for example, you may have to install grunt ala "npm install -g grunt-cli")

*note that because there is no server required to run this project, local SVG resources will not work*